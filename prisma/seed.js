import Prisma from '@prisma/client';

const { PrismaClient } = Prisma;
const prismaClient = new PrismaClient();

const CONTEXTS = [
    { id: 1, name: 'ESG' },
    { id: 2, name: 'Finance' },
    { id: 3, name: 'Generic' },
    { id: 4, name: 'Procurement' },
    { id: 5, name: 'Sales' },
];

const COLLECTIONS = [
    {
        project_id: null,
        name: 'Products',
        description: '',
        context_id: 4,
        status: 'Imported on 00 / 00 / 0000',
        structure: '{"custom":[]}',
    },
    {
        project_id: null,
        name: 'Families',
        description: '',
        context_id: 4,
        status: 'Not initialized',
        structure: '{"custom":[]}',
    },
    {
        project_id: null,
        name: 'Components',
        description: '',
        context_id: 5,
        status: 'Edited on 00 / 00 / 0000 by Username',
        structure: '{"custom":[]}',
    },
    {
        project_id: null,
        name: 'Suppliers',
        description: '',
        context_id: 5,
        status: 'Not initialized',
        structure: '{"custom":[]}',
    },
    {
        project_id: null,
        name: 'Carriers',
        description: '',
        context_id: 5,
        status: 'Not initialized',
        structure: '{"custom":[]}',
    },
];

const DATA_TYPES = [
    {
        name: 'Char',
        description: 'Single line text, max 500 chars',
        order: 1,
    },
    {
        name: 'Float',
        description: 'Floating point number',
        order: 2,
    },
    {
        name: 'Int',
        description: 'Integer number',
        order: 3,
    },
    {
        name: 'Bool',
        description: 'Binary value',
        order: 4,
    },
    {
        name: 'Date',
        description: '',
        order: 5,
    },
    {
        name: 'Timestamp',
        description: '',
        order: 6,
    },
    {
        name: 'Image',
        description: '',
        order: 7,
    },
    {
        name: 'Plain text',
        description: '',
        order: 8,
    },
];

const ENTITIES = [
    {
        project_id: null,
        name: 'Quantity',
        short: 'Q',
        description: 'Quantity',
        type_id: 3,
        unit_id: 1,
        context_id: 2,
    },
    {
        project_id: null,
        name: 'Price',
        short: 'P',
        description: 'Unit price',
        type_id: 2,
        unit_id: 2,
        context_id: 2,
    },
    {
        project_id: null,
        name: 'Cost of goods',
        short: 'COGS',
        description: 'Unit cost of goods sold',
        type_id: 2,
        unit_id: 2,
        context_id: 2,
    },
    {
        project_id: null,
        name: 'Carbon footprint',
        short: 'CFP',
        description: 'Carbon footprint',
        type_id: 2,
        unit_id: 3,
        context_id: 3,
    },
    {
        project_id: null,
        name: 'Gross margin',
        short: 'GM',
        description: 'Gross margin',
        type_id: 2,
        unit_id: 2,
        context_id: 2,
    },
];

const UNITS = [
    {
        name: 'Units',
        short: '#',
        description: '',
    },
    {
        name: 'Euros',
        short: '€',
        description: '',
    },
    {
        name: 'CO2eq',
        short: 'CO2eq',
        description: 'Carbon dioxide equivalent',
    },
    {
        name: 'Kilometers',
        short: 'Km',
        description: '',
    },
    {
        name: 'Kilowatt',
        short: 'kW',
        description: '',
    },
];

async function main() {

    console.log();
    console.log('create contexts...');
    const contextCreated = await prismaClient.contexts.createMany({ data: CONTEXTS });
    console.log('number of context created: ', contextCreated);

    const collectionsData = COLLECTIONS;
    collectionsData[0].structure = JSON.stringify({
        code: true,
        description: true,
        order: false,
        custom: [
            { name: 'List price', type: 'entity', name2: 'Price', id: 3 },
            { name: 'Purchase price', type: 'entity', name2: 'Price', id: 3 },
            { name: 'Family', type: 'collection', name2: 'Family', id: 4 }]
    });

    console.log('\ncreate collections...');
    const collectionsCreated = await prismaClient.collections.createMany({ data: collectionsData });
    console.log('number of collections created: ', collectionsCreated);

    console.log('\ncreate data types...');
    const dataTypesCreated = await prismaClient.data_types.createMany({ data: DATA_TYPES });
    console.log('number of data types created: ', dataTypesCreated);

    console.log('\ncreate units...');
    const unitsCreated = await prismaClient.units.createMany({ data: UNITS });
    console.log('number of units created: ', unitsCreated);

    console.log('\ncreate entities...');
    const entitiesCreated = await prismaClient.entities.createMany({ data: ENTITIES });
    console.log('number of entities created: ', entitiesCreated);
}

main()
    .catch(e => {
        console.error(e)
        process.exit(1)
    })
    .finally(async () => {
        await prismaClient.$disconnect()
    });