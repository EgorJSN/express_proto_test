-- CreateTable
CREATE TABLE `collections` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `project_id` INTEGER,
    `name` VARCHAR(80) DEFAULT '',
    `description` TEXT,
    `context_id` INTEGER,
    `status` VARCHAR(191),
    `structure` JSON NOT NULL,

    UNIQUE INDEX `collections.name_unique`(`name`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `contexts` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(80) DEFAULT '',

    UNIQUE INDEX `contexts.name_unique`(`name`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `data_types` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(80) DEFAULT '',
    `description` TEXT,
    `order` INTEGER,

    UNIQUE INDEX `data_types.name_unique`(`name`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `units` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(80) DEFAULT '',
    `short` VARCHAR(12) DEFAULT '',
    `description` TEXT NOT NULL,

    UNIQUE INDEX `units.name_unique`(`name`),
    UNIQUE INDEX `units.short_unique`(`short`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `entities` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `project_id` INTEGER,
    `name` VARCHAR(80) DEFAULT '',
    `short` VARCHAR(12) DEFAULT '',
    `description` TEXT NOT NULL,
    `type_id` INTEGER,
    `unit_id` INTEGER,
    `context_id` INTEGER,

    UNIQUE INDEX `entities.name_short_unique`(`name`, `short`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `collections` ADD FOREIGN KEY (`context_id`) REFERENCES `contexts`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `entities` ADD FOREIGN KEY (`type_id`) REFERENCES `data_types`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `entities` ADD FOREIGN KEY (`unit_id`) REFERENCES `units`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `entities` ADD FOREIGN KEY (`context_id`) REFERENCES `contexts`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
