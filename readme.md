#Install vendors:

**Note: `node` requirement version > v15**

### `npm i`

### set connection string to .env file (user and password)
in format: `mysql://user_name:user_password@localhost:3306/express_test`

## To migrate all DB tables:
### `npx prisma migrate dev --name init`

## To seed the database, run the DB seed CLI command:

### `npx prisma db seed --preview-feature`

## Start dev server (nodemon):
### `npm run serve`

Runs the app in the development mode.\
Open [http://localhost:5001](http://localhost:5001) to view it in the browser.