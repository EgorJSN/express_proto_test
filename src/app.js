import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import ApiRoutes from "./Routes/ApiRoutes.js";
import { ErrorHandler } from "./Libraries/ErrorHandler.js";

const PORT = process.env.PORT ?? 5001;
const app = express();

dotenv.config();

app.use(cors({
    methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
}));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/', ApiRoutes);

app.get('/', (req, res) => {
    res.send('Express test applications.');
});

app.use(ErrorHandler);

app.listen(PORT, () => {
    console.log(`Express server listen PORT: ${PORT}`);
});