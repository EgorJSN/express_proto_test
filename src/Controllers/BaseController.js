import { matchedData } from "express-validator";

export class BaseController {

    static getRequestValidatedData(req, options) {
        return matchedData(req, options);
    }

}