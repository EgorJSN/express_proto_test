import { BaseController } from "./BaseController.js";
import { ContextsRepository } from "../Repositories/ContextsRepository.js";

export class ContextsController extends BaseController {

    static async list(req, res) {

        const contexts = await ContextsRepository.list();

        res.status(200).json({ contexts });
    }

}