import { body } from "express-validator";
import RequestValidator from "../../../Libraries/RequestValidator.js";

const CollectionsCreateRequestValidator = RequestValidator([
    body('name')
        .isLength({ min: 3 }).withMessage('must be more than 3 characters')
        .notEmpty().withMessage('cannot be empty'),
    body('description').optional({ nullable: true }).isString(),
    body('context_id')
        .isInt()
        .notEmpty().withMessage('context cannot be empty'),
    body('structure').isJSON(),
]);

export default CollectionsCreateRequestValidator;
