import { body } from "express-validator";
import RequestValidator from "../../../Libraries/RequestValidator.js";

const CollectionsUpdateRequestValidator = RequestValidator([
    body('ID').isInt().notEmpty(),
    body('name')
        .isLength({ min: 3 }).withMessage('must be more than 3 characters')
        .notEmpty().withMessage('cannot be empty'),
    body('description').optional({ nullable: true }).isString(),
    body('context_id').isInt().notEmpty(),
    body('structure').isJSON(),
]);

export default CollectionsUpdateRequestValidator;
