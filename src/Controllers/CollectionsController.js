import { BaseController } from "./BaseController.js";
import { CollectionsRepository } from "../Repositories/CollectionsRepository.js";
import CollectionService from "../Services/CollectionService.js";
import { EntitiesRepository } from "../Repositories/EntitiesRepository.js";

export class CollectionsController extends BaseController {

    static async list(req, res) {

        const collections = await CollectionService.list();
        const entities = await EntitiesRepository.list();

        res.status(200).json({
            collections,
            entities,
        });
    }

    static async create(req, res) {

        const collectionData = super.getRequestValidatedData(req);

        const collection = await CollectionService.create(collectionData);

        res.status(201).json({
            collection,
            success: 'success',
        });
    }

    static async update(req, res) {

        const validatedData = super.getRequestValidatedData(req);

        const updatedCollection = await CollectionService.update(validatedData);

        res.status(200).json({
            updatedCollection,
            success: 'updated',
        });
    }

    static async delete(req, res) {

        const id = req.body.id;
        await CollectionsRepository.delete(id);
        await CollectionsRepository.deleteTableByCollectionId(id);

        res.status(200).json({
            success: 'deleted',
        });
    }

}