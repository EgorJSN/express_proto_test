import { validationResult } from "express-validator";
import TranslateService from "../Services/TranslateService.js";

const RequestValidator = validations => {
    return async (req, res, next) => {
        await Promise.all(validations.map(validation => validation.run(req)));

        const errors = validationResult(req);
        if (errors.isEmpty()) {
            return next();
        }

        let errorMessages = errors.array().map((e) => {
            return {
                message: TranslateService.t(e.msg),
                field: e.param,
            }
        });

        res.status(400).json({ errors: errorMessages });
    };
};

export default RequestValidator;