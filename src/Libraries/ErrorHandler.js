import { PrismaErrorHandler } from "../ErrorHandlers/PrismaErrorHandler.js";
import { DefaultErrorHandler } from "../ErrorHandlers/DefaultErrorHandler.js";

const applyErrorHandlers = (error, req, res, ...handlers) => {
    for (let handler of handlers) {
        let handlerResult = handler(error, req, res);

        if (handlerResult) {
            return handlerResult;
        }
    }
}

export const ErrorHandler = (error, req, res, next) => {

    applyErrorHandlers(error, req, res,
        PrismaErrorHandler,
        DefaultErrorHandler
    );
}
