import fs from 'fs';
import path from 'path';

export default class TranslateProvider {

    static translates;

    static getTranslates() {
        if (!this.translates) {
            const __dirname = path.resolve(),
                translateFilePath = path.resolve(
                    __dirname, process.env.TRANSLATES_BASE_DIR + 'en.json'
                );

            this.translates = JSON.parse(fs.readFileSync(translateFilePath));
        }

        return this.translates;
    }
}
