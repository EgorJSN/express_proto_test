import Prisma from '@prisma/client';

export const PrismaErrorHandler = (error, req, res) => {

    if (
        (error instanceof Prisma.PrismaClientKnownRequestError) ||
        (error instanceof Prisma.PrismaClientValidationError)
    ) {
        return res.status(500).send(error.message);
    }
}

