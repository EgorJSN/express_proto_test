export const DefaultErrorHandler = (error, req, res) => {

    return res.status(500).json({
        errorClass: error.constructor.name,
        trace: error.stack.split('\n'),
        errorHandler: 'DefaultErrorHandler',
        message: error.toString(),
    });
}