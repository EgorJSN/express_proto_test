import { BaseRepository } from "./BaseRepository.js";

export class ContextsRepository extends BaseRepository {

    static async list() {
        return await this.db.contexts.findMany();
    }

}