import { BaseRepository } from "./BaseRepository.js";

export class EntitiesRepository extends BaseRepository {

    static async list() {
        return await this.db.entities.findMany();
    }

}