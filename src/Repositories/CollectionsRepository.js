import { BaseRepository } from "./BaseRepository.js";

export class CollectionsRepository extends BaseRepository {

    static async list() {
        return await this.db.collections.findMany({
            include: {
                contexts: true,
            },
        });
    }

    static async getOneById(id) {
        return await this.db.collections.findUnique({
            where: {
                id,
            },
            include: {
                contexts: true,
            },
        });
    }

    static async create(data) {
        return await this.db.collections.create({
            data
        });
    }

    static async deleteTableByCollectionId(id) {
        const tableName = 'C' + id;
        await this.db.$executeRaw(`drop table if exists ${tableName};`);
    }

    static async updateTableBasedOnCollection(collection) {

        const tableName = 'C' + (collection.id ?? collection.ID);
        const structure = JSON.parse(collection.structure);

        let fields = [
            {
                name: 'name',
                type: 'char(80) not null'
            },
        ];

        let constraints = [
            `constraint \`${tableName}.name_unique\` unique(name)`,
        ];

        if (structure.code) {
            fields.push({
                name: 'code',
                type: 'char(20) null',
            });

            constraints.push(
                `constraint \`${tableName}.code_unique\` unique(code)`,
            )
        }

        if (structure.description) {
            fields.push({
                name: 'description',
                type: 'varchar(400) null',
            });
        }

        if (structure.sort) {
            fields.push({
                name: 'sort',
                type: 'int null',
            });
        }

        if (structure.custom?.length) {
            structure.custom.forEach((field, key) => {
                fields.push({
                    name: 'CUSTOM_' + key,
                    type: 'int not null',
                });
            });
        }

        let baseTemplate = `
            create table ${tableName}
            (
                id         int auto_increment primary key,
                {fields},
                {constraints}
            );`;

        const createTableSql = baseTemplate
            .replace('{fields}', fields.map(({name, type}) => name + ' ' + type).join(','))
            .replace('{constraints}', constraints.join(','))
        ;

        console.log('create table sql: ', createTableSql);
        await this.db.$executeRaw(`drop table if exists ${tableName};`);
        await this.db.$executeRaw(createTableSql);
    }

    static async update(id, collection) {

        return await this.db.collections.update({
            where: {
                id,
            },
            data: collection,
        });
    }

    static async delete(id) {

        await this.db.collections.delete({
            where: {
                id,
            }
        });
    }

}