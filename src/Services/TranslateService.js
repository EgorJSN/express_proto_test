import TranslateProvider from "../Libraries/TranslateProvider.js";

const translates = TranslateProvider.getTranslates();

export default class TranslateService {

    static t(label) {
        return translates[label] ?? label;
    }

}