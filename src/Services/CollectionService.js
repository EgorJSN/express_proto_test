import CollectionListTableAdapter from "../Adapters/CollectionListTableAdapter.js";
import { CollectionsRepository } from "../Repositories/CollectionsRepository.js";
import CollectionRowTableAdapter from "../Adapters/CollectionRowTableAdapter.js";

export default class CollectionService {

    static async list() {
        return CollectionListTableAdapter(
            await CollectionsRepository.list()
        );
    }

    static async getOneById(id) {
        let data = await CollectionsRepository.getOneById(id);
        return CollectionRowTableAdapter(data);
    }

    static async update(validatedData) {

        const id = validatedData.ID;
        delete validatedData.ID;

        await CollectionsRepository.update(id, validatedData);
        const collection = await CollectionService.getOneById(id);
        await CollectionsRepository.updateTableBasedOnCollection(collection);

        return collection;
    }

    static async create(validatedData) {

        let collection = await CollectionsRepository.create(validatedData);
        await CollectionsRepository.updateTableBasedOnCollection(collection);

        return await CollectionService.getOneById(collection.id);
    }

}