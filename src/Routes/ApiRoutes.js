import 'express-async-errors';
import { Router } from "express";
import { CollectionsController } from "../Controllers/CollectionsController.js";
import CollectionsCreateRequestValidator from "../Controllers/RequestValidators/Collections/Create.js";
import { ContextsController } from "../Controllers/ContextsController.js";
import CollectionsUpdateRequestValidator from "../Controllers/RequestValidators/Collections/Update.js";

const ApiRoutes = Router();

ApiRoutes
    .route('/error')
    .get((req, res) => {
        throw new Error('test-error-route');
    });

ApiRoutes
    .route('/collections')
    .get(CollectionsController.list);

ApiRoutes
    .route('/contexts')
    .get(ContextsController.list);

ApiRoutes
    .route('/collection')
    .post(CollectionsCreateRequestValidator, CollectionsController.create);

ApiRoutes
    .route('/collection')
    .put(CollectionsUpdateRequestValidator, CollectionsController.update);

ApiRoutes
    .route('/collection')
    .delete(CollectionsController.delete);

export default ApiRoutes;