const rowDataAdapter = (rowData) => {
    return {
        ID: rowData['id'],
        name: rowData['name'],
        description: rowData['description'],
        status: rowData['status'],
        structure: rowData['structure'],
        context_id: rowData['context_id'],
        context: rowData['contexts']['name'],
    }
};

export default function CollectionRowTableAdapter(rowData) {
    return rowDataAdapter(rowData);
}
