import CollectionRowTableAdapter from "./CollectionRowTableAdapter.js";

export default function CollectionListTableAdapter(listData) {
    return listData.map((row) => CollectionRowTableAdapter(row));
}
